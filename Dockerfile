# pull ubuntu image
FROM ubuntu:bionic
USER root

# non-interactive
RUN echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections

# install packages
RUN apt-get update && apt-get -y install sudo
RUN sudo apt-get install -y apt-utils
RUN sudo apt-get install -y build-essential git curl
RUN sudo apt-get install -y openjdk-11-jdk
RUN sudo apt-get update
ENV JAVA_HOME=/usr/lib/jvm/java-11-openjdk-amd64

# install tools
RUN apt-get install -y wget ant maven gradle
RUN sudo apt-get update