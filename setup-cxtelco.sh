#!/bin/bash

WORKDIR=`pwd`

# Copy CXTEL to CXCOMM
for i in "ext-addon" "ext-backoffice" "ext-commerce" "ext-data" "ext-webservices"
do
  EXT_ZIP="ext.zip"
  EXT_TELCO=$WORKDIR/cxtelco/hybris/bin/$i
  EXT_COMM=$WORKDIR/cxcomm/hybris/bin/$i

  cd $EXT_TELCO
  zip -r $EXT_ZIP *
  cp -rv $EXT_TELCO/$EXT_ZIP $EXT_COMM
  unzip -o $EXT_COMM/$EXT_ZIP -d $EXT_COMM
  rm -rf $EXT_TELCO/$EXT_ZIP
  rm -rf $EXT_COMM/$EXT_ZIP
done

cd $WORKDIR