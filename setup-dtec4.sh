#!/bin/bash

WORKDIR=`pwd`

# Copy DTEC4 config files to CXCOMM
cp -rv $WORKDIR/dtec4/config/local.properties $WORKDIR/cxcomm/hybris/config
cp -rv $WORKDIR/dtec4/config/localextensions.xml $WORKDIR/cxcomm/hybris/config

# Copy DTEC4 ext files to CXCOMM
cd $WORKDIR/dtec4/hybris/bin
zip -r crm-integration.zip crm-integration
zip -r custom.zip custom
cp -rv $WORKDIR/dtec4/hybris/bin/crm-integration.zip $WORKDIR/cxcomm/hybris/bin
cp -rv $WORKDIR/dtec4/hybris/bin/custom.zip $WORKDIR/cxcomm/hybris/bin
unzip -o $WORKDIR/cxcomm/hybris/bin/crm-integration.zip -d $WORKDIR/cxcomm/hybris/bin
unzip -o $WORKDIR/cxcomm/hybris/bin/custom.zip -d $WORKDIR/cxcomm/hybris/bin
rm -rf $WORKDIR/cxcomm/hybris/bin/crm-integration.zip
rm -rf $WORKDIR/cxcomm/hybris/bin/custom.zip
rm -rf $WORKDIR/dtec4/hybris/bin/crm-integration.zip
rm -rf $WORKDIR/dtec4/hybris/bin/custom.zip

cd $WORKDIR