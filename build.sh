#!/bin/bash

WORKDIR=`pwd`

# Unzip files
unzip -o $WORKDIR/cxcomm.zip -d $WORKDIR/cxcomm
unzip -o $WORKDIR/cxtelco.zip -d $WORKDIR/cxtelco
unzip -o $WORKDIR/dtec4.zip -d $WORKDIR

# Copy CXTEL to CXCOMM
for i in "ext-addon" "ext-backoffice" "ext-commerce" "ext-data" "ext-webservices"
do
  EXT_ZIP="ext.zip"
  EXT_TELCO=$WORKDIR/cxtelco/hybris/bin/$i
  EXT_COMM=$WORKDIR/cxcomm/hybris/bin/$i
  
  cd $EXT_TELCO
  zip -r $EXT_ZIP *
  cp -rv $EXT_TELCO/$EXT_ZIP $EXT_COMM
  unzip -o $EXT_COMM/$EXT_ZIP -d $EXT_COMM
  rm -rf $EXT_TELCO/$EXT_ZIP
  rm -rf $EXT_COMM/$EXT_ZIP
done

# Copy all ext & platform files from CXCOMM to DTEC4 XXXXXXXXXXXXXXXXX
# cd $WORKDIR/cxcomm/hybris/bin
# zip -r ext.zip *
# cp -rv $WORKDIR/cxcomm/hybris/bin/ext.zip $WORKDIR/dtec4/hybris/bin
# unzip -o $WORKDIR/dtec4/hybris/bin/ext.zip -d $WORKDIR/dtec4/hybris/bin
# rm -rf $WORKDIR/cxcomm/hybris/bin/ext.zip
# rm -rf $WORKDIR/dtec4/hybris/bin/ext.zip

# Enable core dump
ulimit -c unlimited

# Build CXCOMM
cd $WORKDIR/cxcomm/hybris/bin/platform
. ./setantenv.sh
echo 'develop' | ant clean all

# Copy DTEC4 config files to CXCOMM
cp -rv $WORKDIR/dtec4/config/local.properties $WORKDIR/cxcomm/hybris/config
cp -rv $WORKDIR/dtec4/config/localextensions.xml $WORKDIR/cxcomm/hybris/config
dteb2cutilstorefront
# Copy DTEC4 ext files to CXCOMM
cd $WORKDIR/dtec4/hybris/bin
zip -r crm-integration.zip crm-integration
zip -r custom.zip custom
cp -rv $WORKDIR/dtec4/hybris/bin/crm-integration.zip $WORKDIR/cxcomm/hybris/bin
cp -rv $WORKDIR/dtec4/hybris/bin/custom.zip $WORKDIR/cxcomm/hybris/bin
unzip -o $WORKDIR/cxcomm/hybris/bin/crm-integration.zip -d $WORKDIR/cxcomm/hybris/bin
unzip -o $WORKDIR/cxcomm/hybris/bin/custom.zip -d $WORKDIR/cxcomm/hybris/bin
rm -rf $WORKDIR/cxcomm/hybris/bin/crm-integration.zip
rm -rf $WORKDIR/cxcomm/hybris/bin/custom.zip
rm -rf $WORKDIR/dtec4/hybris/bin/crm-integration.zip
rm -rf $WORKDIR/dtec4/hybris/bin/custom.zip

# Build CXCOMM
cd $WORKDIR/cxcomm/hybris/bin/platform
ant clean all


# Add addons
# cd /workspaces/dte-sap-commerce/dtec4/hybris/bin/platform
# ant addoninstall -Daddonnames="dteb2cutiltelcoaddon,smarteditaddon,assistedservicestorefront,dteb2cutilasmaddon,dteb2cutilcontractmgmtaddon,customerticketingaddon" -DaddonStorefront.yacceleratorstorefront="dteb2cutilstorefront"

# in the platform
# ant customize clean all

# start the server
# ./hybrisserver.sh

# https://dte.local:9002/hac --> platfomr --> initialize
# admin/nimda







# Build CXCOMM
# cd /workspaces/dte-sap-commerce/cxcomm/hybris/bin/platform
# . ./setantenv.sh
# echo "develop" | ant server
# ant clean all

# Copy repo files to CXCOMM
# cd /workspaces/dte-sap-commerce
# cp -rv dtec4/hybris/bin/custom cxcomm/hybris/bin
# cp -rv dtec4/hybris/bin/crm-integration cxcomm/hybris/bin

# Copy config files from repo to CXCOMM
# cd /workspaces/dte-sap-commerce
# cp -v dtec4/config/local.properties cxcomm/hybris/config
# cp -v dtec4/config/localextensions.xml cxcomm/hybris/config

# Build CXCOMM
# cd /workspaces/dte-sap-commerce/cxcomm/hybris/bin/platform
# ant clean all