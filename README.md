# DTE - SAP Commerce
This readme includes steps to setup and initialize **DTE Commerce Project** which relies on customized code. 
> [Optional] This repository also includes an example docker file in case you want to run the commerce instance in docker.

## Before you start
The repository only provides docker file and scaffolding scripts to build SAP Commerce, you need to prepare the following files yourself:

| File                      | Availability                                                                  |
|---------------------------|-------------------------------------------------------------------------------|
| SAP Commerce Suite        | ex: CXCOMM201100P_6-70005693.zip                                              |
| SAP Commerce Integration  | ex: CXCOMINTPK210200P_1-80005851.zip                                          |
| B2C Telco Suite           | ex: CXTELPACK2011_0-70006011.zip                                              |
| DTEC4 project source code | available [DTE private GitHub repository](https://github.com/dteenergy/DTEC4) |


## Prepare the files
Download the commerce zip files, put them at the project root path, and rename them as follows:

* SAP Commerce Suite --> `cxcomm.zip`
* SAP Commerce Integration Pack --> `cxcomm-int.zip`
* B2C Telco Suite --> `cxtelco.zip`
* DTEC4 soruce code --> `dtec4.zip`


## Docker settings
The docker file is located at the project root path `./Dockerfile`. You can you any tool or IDE plugin of your choose to build/create docker image/container.

### Recommended docker settings:

| name            | value                                       |
|-----------------|---------------------------------------------|
| Image tag       | dte-commerce-2011                           |
| Container name  | dte-commerce-2011-container                 |
| Bind ports      | 8080:8080, 9002:9002, 8983:8983             |
| Bind mounts     | $PROJECT_ROOT_PATH:/root/dte-commerce-2011/ |

### Recommended docker commands:
* Build image
    ```
    docker build -t dte-commerce-2011 .
    ```
* Create container
    ```
    docker run -it \
    --name dte-commerce-2011-container \
    -p 8080:8080 -p 9002:9002 -p 8983:8983 \
    -v <YOUR_PROJECT_PATH>:/root/dte-commerce-2011/ \
    dte-commerce-2011
    ```
* Start container
    ```
    docker container start -i dte-commerce-2011-container
    ```

## Build and initialize the platform

### Step 1
Execute the set up script. It extracts commerce zip files

* Run `unzip -o cxcomm.zip -d cxcomm`
* Run `unzip -o cxcomm-int.zip -d cxcomm-int`
* Run `unzip -o cxtelco.zip -d cxtelco`
* Run `unzip -o dtec4.zip -d dtec4`

### Step 2
Move modules from other packages to commerce

* Copy all folders in `./cxcomm-int/hybris/bin/modules` to `./cxcomm/hybris/bin/modules/`
* Copy all folders in `./cxtelco/hybris/bin/modules` to `./cxcomm/hybris/bin/modules/`

### Step 3
Run the build. When prompted to choose environment, press `Enter` to continue the build.
```shell
cd ./cxcomm/hybris/bin/platform
. ./setantenv.sh
ant clean all
```

### Step 4
Moves necessary files from `dtec4` to `cxcomm`.

1. Overwrite `/dtec4/config/local.properties` to `cxcomm/hybris/config/local.properties`
2. Overwrite `/dtec4/config/localextensions.xml` to `/cxcomm/hybris/config/localextensions.xml`
3. Copy folder `/dtec4/hybris/bin/custom` to `/cxcomm/hybris/bin`
4. Copy folder `/dtec4/hybris/bin/crm-integration/crm` to `cxcomm/hybris/bin/modules`

Resources:

* [Installing SAP CRM Integration Pack](https://help.sap.com/viewer/2bbc5dc874aa4f568954f2839d0f5687/2005/en-US/b5e7210612b34b19a25e20ca9f061815.html)
* [Creating a New Extension](https://help.sap.com/viewer/b490bb4e85bc42a7aa09d513d0bcb18e/2011/en-US/8b96270b86691014b1c3bbfd7556f0ed.html)

### Step 5
Run the build. When prompted to choose environment, press Enter to continue the build.
```shell
cd ./cxcomm/hybris/bin/platform
ant clean all
```

### Step 6
Install addons
```shell
cd ./cxcomm/hybris/bin/platform
ant addoninstall -Daddonnames="dteb2cutiltelcoaddon,smarteditaddon,assistedservicestorefront,dteb2cutilasmaddon,dteb2cutilcontractmgmtaddon,customerticketingaddon" -DaddonStorefront.yacceleratorstorefront="dteb2cutilstorefront"
```

### Step 7
Add host entries. Add `127.0.0.1 dte.local` to the hosts file. 

* In Windoes, the hosts file is located at `c/WINDOWS/system32/drivers/etc/hosts`
* In MacOS/Linux, the hosts file is located at `/etc/hosts`

### Step 8
Run the final build.
```shell
cd ./cxcomm/hybris/bin/platform
ant customize clean all
```

### Step 9
Set default admin password in `./cxcomm/hybris/config/local.properties`. Add the following to the beginning of the file:
```properties
# Provide default admin password
# https://help.sap.com/viewer/9433604f14ac4ed98908c6d4e7d8c1cc/1905/en-US/d42c4e3fbde74b5eb2056fb1a1b115e8.html
initialpassword.admin=nimda
```

### Step 10
Start the commerce platform:
```shell
cd ./cxcomm/hybris/bin/platform/
./hybrisserver.sh
```

### Step 11
Initialize the platform. Go to `https://dte.local:9002/platform/init`, toggle the following options to "yes", and click "initialize" butoon.

![commerce platform initialize](images/hac-initialize.png)

### Step 12
Import the following ImpEx files at the Backoffice.

1. Go to the ImpEx import section in Backoffice.
2. Copy and paste the content of the ImpEx file.
3. Press "Validate content" button to make sure the content format is valid.
4. Press "Import content".

> If you encounter any error during the importing, you can check commerce's log for more details.

**File 1**
`cxcomm/hybris/bin/custom/dteb2cutil/dteb2cutilstore/resources/dteb2cutilstore/import/sampledata/customizing/customizing.impex`

**File 2**
`cxcomm/hybris/bin/custom/dteb2cutil/dteb2cutilstore/resources/dteb2cutilstore/import/coredata/stores/dte/solr.impex`

![commerce impex import](images/impex-import.png)

### Step 13
Start the Solr server (if it's not started by default)
```shell
cd ./cxcomm/hybris/bin/ext-commerce/solrserver/resources/solr/8.6/server/bin
./solr start -p 8983 -force
```

Here is the default Solr auth info

![solr auth](images/solr-auth.png)

### Step 14
Run Solr indexing in the Backoffice. From the left menu of Backoffice, go to "System" > "Search and Navigation" > "Facet Search Configurations".

You will see two items need to be indexed:

* Solr Config for Backoffice
* utilitiesIndex

![solr indexing backoffice](images/index-backoffice.png)

Select each item and press on the "Index" button in the lower split section. For the first-time initialization, you need to select "full" for your operation. For the subsequent indexing, you can just use either "update" or "delete". Press "START" button to run the indexing.

![run solr indexing](images/run-index-backoffice.png)

One of the common errors that could happen on the local environment is the Solr server is using `http` instead of 'https`. You can get Solr server's url from the log when you start it.

If this happens, you need to update the Solr server url in both config item we mentioned above. Select the config item, go to its "PROPERTIES" in the lower split section, double click on the "Solr server configuration" item.

![go to solr config](images/go-to-solr-config.png)

In the popup window, double-click on the "Endpoint URLs" field and update the URL to the correct one, say, from `https` to `http`. Then press "SAVE" and "REFRESH". Do the same to other Solr config items.

After this, you should now be able to run Solr indexing without problems.

![update solr config](images/update-solr-config.png)

### Step X (Optional)
To remove all the commerce folders and start over, run the following script.
```shell
./clear.sh
```

## Start the platform
### Hybris
Normal mode
```shell
cd ./cxcomm/hybris/bin/platform/
./hybrisserver.sh
```
Debug mode
```shell
cd ./cxcomm/hybris/bin/platform/
./hybrisserver.sh debug
```

### Solr
To start
```shell
cd ./cxcomm/hybris/bin/ext-commerce/solrserver/resources/solr/8.6/server/bin
./solr start -p 8983 -force
```
To stop
```shell
cd ./cxcomm/hybris/bin/ext-commerce/solrserver/resources/solr/8.6/server/bin
./solr stop -p 8983
```

## Encounter license expiration error
https://www.helphybris.com/2018/07/how-to-install-temp-hybris-license.html

## Create products in Backoffice
To show products in the storefront, first we need to have those product data in our Backoffice. You can import the product data directly from CRM. However, if you are running this in local environment, you need to first create the product entries yourself.

On the Backoffice left menu, go to "Catalog" > "Product Offerings", press on the "+" button, and select "Simple Product Offering"".

![go to create product](images/go-to-create-product.png)

The next step is to enter the essential product info. Here we use "PTS_ECO_0.70" product from PTS project as an example. After entering necessary info, just keep pressing "NEXT" button and press "DONE" finally.
> "Article Number" is very important as it's the key of this product

![enter product info](images/enter-product-info.png)

Now we have the product created, but we still can't see it in the storefront until we run Solr indexing again to index the updates. After running the indexing (refer to the previous step), we should be able to see the product shows up in the Backoffice.

![product in backoffice](images/product-in-backoffice.png)

By default, the product is in the `staged` mode, which means users won't see the product in the storefront. To publish updates to the product, select the product, press the synchronize button in the lower split section, select the "online" target, then press "SYNC" button. 

![sync to online](images/sync-to-online.png)

After this step, run Solr indexing again, then you should be able to visit the product detail page in storefront via URL like `https://dte.local:9002/dteb2cutilstorefront/dte/en/p/PTS_ECO_0.70`.
> `/dteb2cutilstorefront` is the URL route for storefront in local environmeent

At this point, although you can visit the product page, the page itself doesn't really have any content. Refer to the next section to see how we update product content via ImpEx file.

## Update product via ImpEx file
Continuing our previous product example "PTS_ECO_0.70", its actual product content is maintained in the following file:

`cxcomm/hybris/bin/custom/dteb2cutil/dteb2cutilstore/resources/dteb2cutilstore/import/sampledata/releases/pts-release/PTS_ECO_0.70.impex`

Here are where the PTS product contents are kept:

![pts product release](images/pts-product-release.png)

To update product "PTS_ECO_0.70", copy and paste the file content of "PTS_ECO_0.70.impex" to hybris admin console, then follow the previously mentioned steps to import.
> If you are running this in local environment, please replace the string `/_ui` with `/dteb2cutilstorefront/_ui` when copy & paste the impex file content, or the images cannot be displayed properly in the storefront.

After importing the impex file, run Solr indexing again, then you should be able to see the correct content on the product page.

![correct pdp](images/correct-pdp.png)

## Other

### PTS products ImpEx files
```shell
cxcomm/hybris/bin/custom/dteb2cutil/dteb2cutilstore/resources/dteb2cutilstore/import/sampledata/releases/pts-release
```